import java.util.ArrayList;

/**
 * 
 * @author EdwardBurke
 */

public class Players
{
    private ArrayList<Player> players;
    
    
    public Players()
    {
        players = new ArrayList<Player>();
    }

    
    public ArrayList<Player> getPlayers()
    {
    	return players;
    }
    
    
    
    public void add (Player player)
    {
        players.add (player); 
    }
    
    
    
    public void remove (Player player)
    {
    	if (player.getGrossProfit() < 0)
    	{
    	players.remove(player);
    	}
    }
    
    
    
    public void next (Player player)
    {
    	int index = 0;
    	index++;
    	}

    
    
    
    public String listPlayers()
    {
        String listOfPlayers = "";
        int index = 0;
        for (Player player: players)
        {
            listOfPlayers = listOfPlayers + index + ": " + player.toString() + "\n";
            index++;
        }
        if (listOfPlayers.equals(""))
        {
            return "No players are playing!";
        }
        else
        {
            return listOfPlayers;
        }
        
    }

    
    
    public String richestPlayer()
    {
        if (players.size() > 0)
        {
            Player richestPlayer = players.get(0);
            for (Player player : players)
            { 
                if (player.getGrossProfit() > richestPlayer.getGrossProfit() )  
                    richestPlayer = player;
            }
            return richestPlayer.getPlayerName();
        }
        else 
            return "No Players are in the game!";
    } 
    
    
    
    public String poorestPlayer()
    {
        if (players.size() > 0)
        {
            Player richestPlayer = players.get(0);
            for (Player player : players)
            { 
                if (player.getGrossProfit() < richestPlayer.getGrossProfit() )  
                    richestPlayer = player;
            }
            return richestPlayer.getPlayerName();
        }
        else 
            return "No Players are in the game!";
    } 
    
    
    
    public String listAllPlayers()
    {
        String str = "";
        
        for (Player player : players)
        {          
                str += "Player(name): " + player.getPlayerName() + "(" 
                    + player.getPlayerName() + "): €" + player.getGrossProfit() + "\n";   
        }

        if (str.equals(""))
        {
            return "No Players are currently playing!";
        }
        else 
            return str;
    }      
}