import java.util.HashMap;

public class Board {
	
	HashMap<String,Land> tileMap;
	private enum Quad{
		NW, SW, SE, NE
	}
	Board(){
		tileMap = new HashMap<String,Land>();
		for(Quad i: Quad.values()){ // for each values in quadrant, so  NW, SW, SE, NE
			for(int x=-1;x<=1;x++){ // for the following values of x: -1  0 1
				for(int y=-1;y<=7;y++){ //for the following values of y: -1  0 1
					if(!(x==y && x!=0)){    // Here is some condition for whatever reason, let's list the cases
						// x == -1 and y == -1      => x == y: true and j != 0 is true, so !(x==y && x!=0) is FALSE => nothing happens
						//  x == -1 and y == 0       => x == y: FALSE so  !(x==y && x!=0) is TRUE, first case is "i = 'NW'" and "x = -1" and "y = 0"
						String key = "" + i + x + y;
						tileMap.put(key, new Land());
						
					}
				}
			}

		}


		   
	}
	public void purchase(Player player, String placement){
		if(tileMap.containsKey(placement)){
			Land land = tileMap.get(placement);
			player.addConcession(placement); 
			land.purchase(player);
		}
	}
	public void sell(Player player, String placement){
		if(tileMap.containsKey(placement)){
			Land land = tileMap.get(placement);
			player.dropConcession(placement); 
			land.sell(player);
		}
	}

	public String toString(){
		return tileMap.toString();
	}
}