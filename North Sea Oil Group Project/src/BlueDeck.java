import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class BlueDeck {
	
	private int randomNum = ThreadLocalRandom.current().nextInt(0, 7 + 1);
	private static int get(int randomNum) {
		return randomNum;
	}
	

	void fillDeck(){
		ArrayList<BlueCard> blueDeck = new ArrayList<>();  ///field in the class
		blueDeck.add(new BlueCard("Shallow Water",20000));
		blueDeck.add(new BlueCard("Shallow Water", 40000));
		blueDeck.add(new BlueCard("Reefs", 20000));
		blueDeck.add(new BlueCard("Reefs", 40000));
		blueDeck.add(new BlueCard("Reefs", 100000));
		blueDeck.add(new BlueCard("Deep Water", 20000));
		blueDeck.add(new BlueCard("Deep Water", 40000));
		blueDeck.add(new BlueCard("Deep Water", 100000));
		}
	
	int dealBlueCard()
	{
		int blueCard = BlueDeck.get(randomNum);
		System.out.println(blueCard);
		return blueCard;
	}
	
	//String blueCard = BlueDeck.get(Random);
	//System.out.println(blueCard)
	//to be added after the dealCard method 
}
