
/* Author: EdwardBurke,
 * 
 * 
 */

public class Player {


	    private String playerName;
	    private int moneyBorrowed;
	    private int grossProfit;

	    
		public Player(String playerName, int moneyBorrowed, int grossProfit)
	    {
	        this.playerName = playerName;
	        this.moneyBorrowed = moneyBorrowed;
	        this.grossProfit = grossProfit;
	    }
	    
		
		
	    public Player()
	    {
	    }
	    
	    
	    
	    public String getPlayerName()
	    {
	        return playerName;
	    }
	    
	    
	    
	    public int getMoneyBorrowed()
	    {
	        return moneyBorrowed;
	    }

	    
	    
	    public int getGrossProfit()
	    {
	        return grossProfit;
	    }
	    
	    
	    
	    public int getDrillsOwned
	    

	    
		public void setPlayerName(String playerName) {
			this.playerName = playerName;
		}

		
		
		public void setMoneyBorrowed(int moneyBorrowed) {
			this.moneyBorrowed = moneyBorrowed;
		}

		
		
		public void setGrossProfit(int grossProfit) {
			this.grossProfit = grossProfit;
		}


		
	    public String toString()
	    {
	        return "Player Name " + playerName
	             + ", Loaned Money: " + moneyBorrowed
	             + ", Total Money: " + grossProfit;
	    }
	    
	}