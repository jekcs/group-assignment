import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class BrownDeck {

	private int randomNum = ThreadLocalRandom.current().nextInt(0, 12 + 1);
	private static int get(int randomNum) {
		return randomNum;
	}

	void fillDeck(){

		ArrayList<BrownCard> brownDeck = new ArrayList<>();
		brownDeck.add(new BrownCard("Spring", "good", "good", "good", "good"));
		brownDeck.add(new BrownCard("Spring", "storm", "rough", "storm", "rough"));
		brownDeck.add(new BrownCard("Spring", "rough", "rough", "good", "good"));
		brownDeck.add(new BrownCard("Summer", "good", "good", "good", "good"));
		brownDeck.add(new BrownCard("Summer", "storm", "rough", "storm", "good"));
		brownDeck.add(new BrownCard("Autumn", "good", "rough", "good", "good"));
		brownDeck.add(new BrownCard("Autumn", "rough", "good", "good", "good"));
		brownDeck.add(new BrownCard("Autumn", "good", "good", "good", "good"));
		brownDeck.add(new BrownCard("Winter", "good", "good", "good", "good"));
		brownDeck.add(new BrownCard("Winter", "rough", "gale", "rough", "gale"));
		brownDeck.add(new BrownCard("Winter", "gale", "gale", "storm", "storm"));
		brownDeck.add(new BrownCard("Winter", "gale", "rough", "gale", "rough"));
		

}
	
	int dealBrownCard()
	{
		int brownCard = BrownDeck.get(randomNum);
		System.out.println(brownCard);
		return brownCard;
	}
}