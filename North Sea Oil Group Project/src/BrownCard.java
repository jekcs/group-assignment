public class BrownCard {
	private String season;
	private String conditionNW;
	private String conditionNE;
	private String conditionSW;
	private String conditionSE;
	
	//getters and setters
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getConditionNW() {
		return conditionNW;
	}
	public void setConditionNW(String conditionNW) {
		this.conditionNW = conditionNW;
	}
	public String getConditionNE() {
		return conditionNE;
	}
	public void setConditionNE(String conditionNE) {
		this.conditionNE = conditionNE;
	}
	public String getConditionSW() {
		return conditionSW;
	}
	public void setConditionSW(String conditionSW) {
		this.conditionSW = conditionSW;
	}
	public String getConditionSE() {
		return conditionSE;
	}
	public void setConditionSE(String conditionSE) {
		this.conditionSE = conditionSE;
	}
	
	//toString
	@Override
	public String toString() {
		return "BrownCard [season=" + season + ", conditionNW=" + conditionNW + ", conditionNE=" + conditionNE
				+ ", conditionSW=" + conditionSW + ", conditionSE=" + conditionSE + "]";
	}
	
	//Array constructor
	public BrownCard(String season, String conditionNW, String conditionNE, String conditionSW, String conditionSE) {
		super();
		this.season = season;
		this.conditionNW = conditionNW;
		this.conditionNE = conditionNE;
		this.conditionSW = conditionSW;
		this.conditionSE = conditionSE;
	}
	
	
	

	
	
}
