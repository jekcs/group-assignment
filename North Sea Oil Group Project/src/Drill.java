
public class Drill {
		
	private enum drillSize {
		Shallow_Water, Reefs, Deep_Water
	}
	
	public static Drill[] drills = new Drill[] { new Drill(0), new Drill(0), new Drill(1), new Drill(1), new Drill(2), new Drill(2)};
	private drillSize size;
	private Player owner;
	private Land land;
	Drill(int size){
		this.size = drillSize.values()[size];
	}
	
	public void purchase(Player owner){
		this.owner = owner;
	}
	
	public void place(Land land){
		this.land = land;
	}
	
	public int getSize(){
		return this.size.ordinal();
	}
	
	public Player getOwner(){
		return this.owner;
	}
	
	public Land getLand(){
		return this.land;
	}
	
	public static void drillPhase() {
		System.out.print("drillPhase");
		
	}
}
